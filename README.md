# Portfolio
Un site présentant mon cv et mes réalisations

# Prérequis
* npx create-react-app
* docker

# Construire l'image docker

```bash
docker build --tag my_portfolio:1.0 .
```
# Vérifier que l'image "my_portfolio" est bien construite
```bash
docker images
```
# Exécuter l'image pour créer le conteneur
```bash
docker run -p 4250:80 -d --name myportolio_container my_portfolio:1.0
```
L'application sera déployée sur localhost:4250
