import React from "react";
import Title from "../Components/Title";
import Location from "../Components/Location";
import ContactForm from "../Components/ContactForm";

const Contact = () => {
  return (
    <section className="contact">
      <div className="container">
        <Title>Contact</Title>

        <div className="row">
          <Location />
          <ContactForm />
        </div>
      </div>
    </section>
  );
};

export default Contact;
