import React from "react";
import ProgressCircle from "../Components/ProgressCricle";
import Title from "../Components/Title";
import { knowledgesData } from "../data/knowledgesData";
import cv from "../media/cv_naim_yazid.png";

const Knowledges = () => {
  return (
    <section
      className="knowledges"
    >
      <div className="container">
        <Title paragraph='Ces Compétences sont en constantes améliorations car pour moi la
            joie de ce métier fait que l’on en apprend tous les jours, d’autant
            plus en étant impliqué, curieux et passionné.'>Compétences</Title>

        <div className="row d-flex align-items-start">
          <div
            className="col-lg-6"
            data-aos="fade-right"
            data-aos-duration="400"
            data-aos-easing="linear"
          >
            {knowledgesData &&
              knowledgesData.map((knowledge) => {
                return (
                  <div
                    className="knowledges-item"
                    key={knowledge.id}
                  >
                    <div className="Knowledges-content">
                      <div className="knowledges-title">
                        <div className="box-icon">{knowledge.iconTitle}</div>
                        <h3>{knowledge.title}</h3>
                      </div>
                      {knowledge.description ? (
                        <ul>{knowledge.description}</ul>
                      ) : (
                        ""
                      )}
                      <div className="knowledges-icons">
                        {knowledge.icons.map((icon, index) => {
                          return (
                            <ProgressCircle
                              key={index}
                              children={icon.icon}
                              percent={icon.percent}
                            />
                          );
                        })}
                      </div>
                    </div>
                  </div>
                );
              })}
          </div>
          <div
            className="curriculum col-lg-6"
            data-aos="fade-left"
            data-aos-duration="400"
            data-aos-easing="linear"
          >
            <img className="img-fluid" src={cv} alt="Curriculum Vitae" />
            <p>
              Vous avez une préférence pour les CVs Classiques? Vous pouvez le
              télécharger par ici :
            </p>
            <div className="text-center">
              <a
                href="./media/cv_naim_yazid.pdf"
                download="cv_naim_yazid.pdf"
                className="btn"
              >
                Télécharger CV
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Knowledges;
