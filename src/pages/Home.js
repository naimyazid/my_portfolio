import React from "react";
import Typed from "typed.js";
import { useEffect, useRef } from "react";

const Home = () => {
  const el = useRef(null);

  useEffect(() => {
    const typed = new Typed(el.current, {
      strings: [
        "Développeur junior",
        "Back-end Symfony",
        "Front-end ReactJs",
      ],
      startDelay: 300,
      typeSpeed: 100,
      backSpeed: 100,
      backDelay: 1000,
      smartBackspace: true,
      loop: true,
      showCursor: true,
    });
    return () => {
      typed.destroy();
    };
  }, []);

  return (
    <section
      style={{ marginLeft: 0 }}
      id="home"
      className="d-flex flex-column justify-content-center align-items-center"
    >
      <div className="home-container">
        <h1>Naim Yazid</h1>
        <p>
          <span ref={el}></span>
        </p>
      </div>
    </section>
  );
};

export default React.memo(Home);
