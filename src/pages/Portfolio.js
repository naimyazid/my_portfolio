import { React, useState } from "react";
import { portfolioData } from "../data/portfolioData";
import Title from "../Components/Title";
import ProjectList from "../Components/ProjectList";
import ProjectDetails from "../Components/ProjectDetails";
import Radio from "../Components/Filter";

const Portfolio = () => {
  const storedProjects = portfolioData;
  const filtersArray = [
    { id: 1, value: "Css" },
    { id: 2, value: "Javascript" },
    { id: 3, value: "Php" },
    { id: 4, value: "React" },
    { id: 5, value: "Symfony" },
  ];

  const [showDetails, setShowDetails] = useState(false);
  const [projectId, setProjectId] = useState(null);
  const [filter, setFilter] = useState("Css");

  const handleFilter = (e) => {
    let filterItem = e.target;
    const portfolioFilters = document.querySelectorAll(".portfolio-flters li");
    portfolioFilters.forEach((portfolioFilter) =>
      portfolioFilter.classList.remove("filter-active")
    );
    filterItem.classList.add("filter-active");
    setFilter(filterItem.getAttribute("data-filter"));
  };

  const handleShowDetails = (id = null) => {
    setProjectId(id);
    setShowDetails(!showDetails);
  };

  const portfolioElement = !showDetails ? (
    <section
      className="portfolio section-bg"
    >
      <div className="container">
        <Title>Projets</Title>

        <Radio filtersArray={filtersArray} handleFilter={handleFilter} filter={filter} />

        <ProjectList 
          portfolioData={portfolioData}
          filter={filter}
          handleShowDetails={handleShowDetails}
        />
      </div>
    </section>
  ) : (
    <ProjectDetails
      handleShowDetails={handleShowDetails}
      projectDetails={storedProjects[projectId]}
    />
  );

  return <>{portfolioElement}</>;
};

export default Portfolio;
