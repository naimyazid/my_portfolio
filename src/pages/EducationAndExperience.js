import React from "react";
import Resume from "../Components/Resume";
import { educationData } from "../data/formationsData";
import { experienceData } from "../data/experiencesData";
import Title from "../Components/Title";

const EducationAndExperience = () => {
  return (
    <section
      className="resume"
    >
      <div className="container">
        <Title>Parcours</Title>

        <div className="row gx-0">
          <div
            className="col-lg-6 pe-md-3"
            data-aos="fade-right"
            data-aos-duration="400"
            data-aos-easing="linear"
          >
            <h3 className="resume-title">Expérience professionnelle</h3>
            {experienceData &&
              experienceData.map((experienceItem) => {
                return (
                  <Resume key={experienceItem.id} {...experienceItem}></Resume>
                );
              })}
          </div>
          <div
            className="col-lg-6 ps-lg-3"
            data-aos="fade-left"
            data-aos-duration="400"
            data-aos-easing="linear"
          >
            <h3 className="resume-title">Formation</h3>
            {educationData &&
              educationData.map((educationItem) => {
                return (
                  <Resume key={educationItem.id} {...educationItem}></Resume>
                );
              })}
          </div>
        </div>
      </div>
    </section>
  );
};

export default EducationAndExperience;
