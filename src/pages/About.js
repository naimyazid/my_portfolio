import React from "react";
import profileImage from "../media/photo_identite_mini.jpg";
import { AiOutlineDoubleRight } from "react-icons/ai";
import Title from "../Components/Title";

const About = () => {
  return (
    <section
      className="about"
    >
      <div className="container">
        <Title>Présentation</Title>

        <div className="row">
          <div
            className="col-lg-4"
            data-aos="fade-right"
            data-aos-duration="400"
            data-aos-easing="linear"
          >
            <img src={profileImage} className="img-fluid" alt="profile" />
          </div>
          <div
            className="col-lg-8 pt-4 pt-lg-0 content"
            data-aos="fade-left"
            data-aos-duration="400"
            data-aos-easing="linear"
          >
            <h3>Développeur fullstack junior</h3>
            <div className="row mb-3">
              <div className="col-lg-6">
                <ul>
                  <li>
                    <AiOutlineDoubleRight />
                    <strong>Age:</strong> <span>37 ans</span>
                  </li>
                  <li>
                    <AiOutlineDoubleRight />
                    <strong>Adresse email:</strong>
                    <span>naimyazid@gmail.com</span>
                  </li>

                  <li>
                    <AiOutlineDoubleRight />
                    <strong>Adresse:</strong>
                    <span>Saint-Denis, Ile-De-France</span>
                  </li>
                </ul>
              </div>
              <div className="col-lg-6">
                <ul>
                  <li>
                    <AiOutlineDoubleRight />
                    <strong>Téléphone:</strong> <span>06 64 87 86 80</span>
                  </li>
                  <li>
                    <AiOutlineDoubleRight />
                    <strong>Site web:</strong> <span>www.naim-yazid.fr</span>
                  </li>
                  <li>
                    <AiOutlineDoubleRight />
                    <strong>Mobilité:</strong>
                    <span>Permis B | Possède un véhicule</span>
                  </li>
                </ul>
              </div>
            </div>
            <p>
              Passioné d'informatique et de nouvelles technologies depuis
              toujours, le monde du web est mon domaine de prédilection. J'ai
              commencé en autodidacte avant de valider le titre de développeur
              web et web mobile au sein du Greta des Hauts-de-Seine. Curieux et
              rigoureux, je m'adapte en toute circonstance. Si vous êtes à la
              recherche d’un collaborateur dans le domaine du développement
              front-end ou back-end, vous pouvez compter sur mes compétences
              pour vous donner satisfaction.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
