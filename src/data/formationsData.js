export const educationData = [
  {
    id: 1,
    name: "Titre développeur web et web mobile",
    institution: "Greta - 92",
    year: 2021,
    description:
      "Maîtriser toute la chaine de développement (front-end et back-end). Développer une application web dynamique et responsive design. Maîtriser les différentes phases de conception d'une base de données relationnelle.",
  },
  {
    id: 2,
    name: "Licence de littérature italienne",
    institution: "Université Paris 8",
    year: 2011,
    description:
      "Acquisition de compétences linguistiques en langue italienne et une bonne connaissance en littérature, civilisation et culture italiennes.",
  },
  {
    id: 3,
    name: "Licence langues étrangères (Lea-Italien)",
    institution: "Université d'Alger",
    year: 2007,
    description:
      "Apprentissage progressif des bases de la langue italienne. Développement des facultés de compréhension globale, d’expression écrite et orale.",
  },
  {
    id: 4,
    name: "Bac",
    institution: "Lycée des martyrs Oudjeddi (Algérie)",
    year: 2003,
    description: "Bac général, option sciences de la nature et de la vie.",
  },
];
