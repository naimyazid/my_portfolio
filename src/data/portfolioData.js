export const portfolioData = [
  {
    id: 1,
    name: "Galaxy Swiss Bourdin",
    technos: ["Css", "Bootstrap", "Javascript", "JQuery", "Php", "Symfony"],
    languages: ["Css", "Javascript", "Symfony"],
    source: "https://gitlab.com/naimyazid/galaxyswissbourdin",
    site: "https://www.gsb.naim-yazid.fr",
    info: "Projet réalisé dans le cadre de ma formation de développeur web au sein du Greta92. L’objectif de ce projet est de mettre en place une plateforme web qui permet au groupe pharmaceutique Galaxy Swiss Bourdin de pouvoir centraliser son système de gestion de frais. L'application doit permettre aux visiteurs médicaux de pouvoir enregistrer tout frais engagé, aussi bien pour l’activité directe (déplacement, restauration, et hébergement) que pour les activités annexes (événementiel, conférences ou autres); ces frais devront ensuite être validés par un comptable.",
    pictures: [
      "./media/saisie_frais_gsb.png",
      "./media/traiter_frais_gsb.png",
      "./media/back_office_gsb.png",
    ],
  },
  {
    id: 2,
    name: "PictoPicto",
    technos: ["Css", "Bootstrap", "Javascript", "JQuery", "Php", "Symfony"],
    languages: ["Css", "Javascript", "Symfony"],
    source: "",
    site: "http://formation-informatiques.fr",
    info: "Projet réalisé pendant mon stage à l'ECAM-EPMI de Cergy-Pontoise. J'ai travaillé sur une application pensée par Mr Benkherrat Moncef, chercheur à l’ECAM-EPMI. L’objectif de cette application est d’aider les enfants atteints du syndrome de l’autisme dans leur communication. Ils peuvent construire des phrases simples, grâce à un système de glisser-déposer de pictogrammes. J’ai été chargé de développer une fonctionnalité, qui consiste à permettre à un utilisateur, en l’occurrence, un enfant autiste, d'effectuer un enregistrement audio; et à l’administrateur, qui peut être un éducateur spécialisé ou un orthophoniste, d’afficher le spectrogramme du fichier audio enregistré par l’enfant et de pouvoir ainsi le confronter avec le spectrogramme d’un enregistrement audio qu’il aurait éffectué.",
    pictures: [
      "./media/pictopicto.jpg",
      "./media/audio_pictopicto.jpg",
      "./media/spectrogram_pictopicto.jpg",
    ],
  },
  {
    id: 3,
    name: "Portfolio",
    technos: ["Css", "Bootstrap", "React"],
    languages: ["Css", "React"],
    source: "https://gitlab.com/naimyazid/my_portfolio",
    site: "https://www.naim-yazid.fr",
    info: "Projet personnel, réalisé avec React.Js pour présenter les compétences acquises tout au long de ma formation et mes réalisations.",
    pictures: [
      "./media/accueil_portfolio.jpg",
      "./media/apropos_portfolio.jpg",
      "./media/trello_portfolio.jpg",
    ],
  },
];

