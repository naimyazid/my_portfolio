export const experienceData = [
  {
    id: 1,
    name: "Développeur web (stagiaire)",
    institution: "Ecam-Epmi (Cérgy-Pontoise)",
    year: "février 2021 - avril 2021",
    description:
      "Mise en place d'un système d'enregistrement vocal et l'affichage d'un spectrogramme. Pour la prtie back, j'ai utilisé le framework Symfony. Côté front, j'ai utilisé Bootstrap, JQuery, wavesurfer.js et Fr.voice.js.",
  },
  {
    id: 2,
    name: "Adjoint de directeur",
    institution: "Leader Price Exploitation",
    year: "2015 - 2019",
    description:
      "Réalisation des plannings du personnel; approvisionnement des rayons (commande en RAO); tâches administratives (facturation, moyens de paiement); suivi du respect de la politique de l’entreprise; réalisation d’inventaires.",
  },
  {
    id: 4,
    name: "Employé libre service",
    institution: "Leader Price Sesadis",
    year: "2009 - 2015",
    description:
      "Réception et mise en rayon des produits; accueil et orientation des clients; encaissement des clients; entretien de la surface de vente.",
  },
];
