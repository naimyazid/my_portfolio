import {
  SiAdobexd,
  SiAdobeillustrator,
  SiAdobephotoshop,
  SiPhp,
  SiRobotframework,
  SiMysql,
  SiFirebase,
} from "react-icons/si";
import { BiCodeAlt } from "react-icons/bi";
import {
  FaHtml5,
  FaCss3Alt,
  FaReact,
  FaSymfony,
  FaElementor,
  FaWordpress,
  FaGithub,
  FaGitlab,
  FaDocker,
  FaTrello,
  FaGitAlt,
} from "react-icons/fa";
import {
  DiJavascript,
  DiBootstrap,
  DiJqueryLogo,
  DiDatabase,
  DiScrum,
} from "react-icons/di";
import { AiOutlineConsoleSql } from "react-icons/ai";
import { IoLogoSass } from "react-icons/io";
import {
  MdOutlineDesignServices,
  MdOutlineSettingsApplications,
} from "react-icons/md";
import { CgFigma } from "react-icons/cg";
import { RiMentalHealthFill } from "react-icons/ri";

export const knowledgesData = [
  {
    id: 1,
    title: "Langages",
    iconTitle: <BiCodeAlt />,
    icons: [
      {
        icon: <FaHtml5 />,
        percent: 80,
      },
      {
        icon: <FaCss3Alt />,
        percent: 80,
      },
      {
        icon: <DiJavascript />,
        percent: 70,
      },
      {
        icon: <SiPhp />,
        percent: 70,
      },
      {
        icon: <AiOutlineConsoleSql />,
        percent: 60,
      },
    ],
  },
  {
    id: 2,
    title: "Frameworks & Librairies",
    iconTitle: <SiRobotframework />,
    icons: [
      {
        icon: <DiBootstrap />,
        percent: 80,
      },
      {
        icon: <IoLogoSass />,
        percent: 70,
      },
      {
        icon: <DiJqueryLogo />,
        percent: 70,
      },
      {
        icon: <FaReact />,
        percent: 60,
      },
      {
        icon: <FaSymfony />,
        percent: 60,
      },
    ],
  },
  {
    id: 3,
    title: "Versionning - DevOps - Agilité",
    iconTitle: <FaGitAlt />,
    icons: [
      {
        icon: <FaGithub />,
        percent: 60,
      },
      {
        icon: <FaGitlab />,
        percent: 60,
      },
      {
        icon: <FaDocker />,
        percent: 40,
      },
      {
        icon: <FaTrello />,
        percent: 70,
      },
      {
        icon: <DiScrum />,
        percent: 30,
      },
    ],
  },
  {
    id: 4,
    title: "Bases de données",
    iconTitle: <DiDatabase />,
    icons: [
      {
        icon: <SiMysql />,
        percent: 60,
      },
      {
        icon: <SiFirebase />,
        percent: 40,
      },
    ],
  },
  {
    id: 5,
    title: "Conception graphique",
    iconTitle: <MdOutlineDesignServices />,
    icons: [
      {
        icon: <SiAdobephotoshop />,
        percent: 50,
      },
      {
        icon: <SiAdobeillustrator />,
        percent: 40,
      },
      {
        icon: <SiAdobexd />,
        percent: 80,
      },
      {
        icon: <CgFigma />,
        percent: 70,
      },
    ],
  },
  {
    id: 6,
    title: "CMS & Constructeur de pages",
    iconTitle: <MdOutlineSettingsApplications />,
    icons: [
      {
        icon: <FaWordpress />,
        percent: 50,
      },
      {
        icon: <FaElementor />,
        percent: 70,
      },
    ],
  },
  {
    id: 7,
    title: "Personnelle",
    iconTitle: <RiMentalHealthFill />,
    description: [
      <li key="1">
        Tres bon sens relationnel et aisance dans le travail en équipe.
      </li>,
      <li key="2">Gestion de projet en groupe.</li>,
      <li key="3">Travail en autonomie et haute capacité d'apdaptation.</li>,
    ],
    icons: [],
  },
];
