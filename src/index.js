import React from "react";
import ReactDOM from "react-dom";
import App from "./Components/App";
import "./css/custom.scss";
import reportWebVitals from "./reportWebVitals";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
