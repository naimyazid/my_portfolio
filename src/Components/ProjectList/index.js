import React from "react";
import Project from "../Project";

const ProjectList = ({ portfolioData, filter, handleShowDetails }) => {
  return (
    <div className="row">
      {
        portfolioData
          .filter((projectItem) => projectItem.languages.includes(filter))
          .map((projectItem, index) => (
            <Project
              key={projectItem.id}
              {...projectItem}
              index={index}
              handleShowDetails={handleShowDetails}
            />
          ))}
    </div>
  );
};

export default ProjectList;
