import React from "react";
import CookieConsent from "react-cookie-consent";
import { NavLink } from "react-router-dom";

const Cookie = () => {
  return (
    <div className="container">
      <div className="row">
        <CookieConsent
          location="bottom"
          buttonText="Autoriser les cookies"
          cookieName="consentCookies"
          style={{
            background: "#2c2f3f",
            fontSize: "14px",
            paddingTop: "20px",
            paddingBottom: "20px",
          }}
          buttonStyle={{
            backgroundColor: "#149ddd",
            color: "#fff",
            fontSize: "13px",
          }}
          expires={150}
        >
          Nous utilisons des cookies pour des tâches de fond comme la
          l'analyse du trafic et l'amélioration de notre site.
          <span style={{ display: "block" }}>
            Pour plus d'informations, voir notre
            <NavLink
              style={{
                display: "inline",
                padding: "0 5px",
                color: "#149ddd",
              }}
              exact
              to="/mentions légales"
              className="nav-link"
            >
              politique de confidentialité.
            </NavLink>
          </span>
        </CookieConsent>
      </div>
    </div>
  );
};

export default Cookie;
