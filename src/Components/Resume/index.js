import React from "react";

const Resume = ({ name, institution, year, description }) => {
  return (
    <>
      <div className="resume-item">
        <h4>{name}</h4>
        <h5>{year}</h5>
        <p>{institution}</p>
        <p>{description}</p>
      </div>
    </>
  );
};

export default Resume;
