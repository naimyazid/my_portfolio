import { React, useState } from "react";
import emailjs from "@emailjs/browser";
import Recaptcha from "react-recaptcha";


const ContactForm = () => {

  const TEMPLATE_ID = process.env.REACT_APP_TEMPLATE_ID;
  const SERVICE_ID = process.env.REACT_APP_SERVICE_ID;
  const USER_ID = process.env.REACT_APP_USER_ID;
  const RECAPTCHA_KEY = process.env.REACT_APP_RECAPTCHA_KEY;

  

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [captchaValue, setCaptchaValue] = useState("");
  const [showCaptcha, setShowCaptcha] = useState(true);

  const isEmail = () => {
    let mail = document.querySelector(".not-mail");
    let regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

    if (email.match(regex)) {
      mail.style.display = "none";
      return true;
    } else {
      mail.style.display = "block";
      mail.style.animation = "dongle 1s";
      setTimeout(() => {
        mail.style.animation = "none";
      }, 1000);
      return false;
    }
  };

  const failMessage = (message) => {
    let formMessage = document.querySelector(".form_message");
    formMessage.innerHTML = message;
    formMessage.style.opacity = "1";

    document.getElementById("name").classList.add("error");
    document.getElementById("email").classList.add("error");
    document.getElementById("message").classList.add("error");
  };

  const successMessage = () => {
    let formMessage = document.querySelector(".form_message");
    formMessage.innerHTML = "Votre message a été envoyé avec succès !";
    formMessage.style.color = "#28a745";
    formMessage.style.opacity = "1";

    document.getElementById("name").classList.remove("error");
    document.getElementById("email").classList.remove("error");
    document.getElementById("message").classList.remove("error");

    setTimeout(() => {
      formMessage.style.opacity = "0";
    }, 3000);
  };

  const sendFeedback = (templateId, variables) => {
    emailjs
      .send(
        SERVICE_ID,
        templateId,
        variables,
        USER_ID
      )
      .then((res) => {
        successMessage();
        setName("");
        setEmail("");
        setMessage("");
        setCaptchaValue("");
        setShowCaptcha(false);
      })
      .catch(() => {
        failMessage("Une erreur s'est produite, veuillez réessayer.");
      });
  };

  const recaptchaLoded = () => {
    console.log("Done!!!!");
  };

  const verifyCallback = (response) => {
    if (response) {
      console.log(response);
      setCaptchaValue(response);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (name && isEmail() && message) {
      sendFeedback(TEMPLATE_ID, {
        name,
        email,
        message,
        "g-recaptcha-response": captchaValue,
      });
    } else {
      failMessage("Merci de remplir correctement les champs requis *");
    }
  };

  return (
    <div
      className="col-lg-7 mt-5 mt-lg-0 d-flex justify-content-center align-items-stretch"
      data-aos="fade-left"
      data-aos-duration="400"
      data-aos-easing="linear"
    >
      <form className="form text-center">
        <div className="form-group py-2">
          <input
            type="text"
            id="name"
            name="name"
            onChange={(e) => setName(e.target.value)}
            placeholder="Nom *"
            value={name}
            autoComplete="off"
          />
        </div>
        <div className="form-group position-relative py-2">
          <input
            className="input"
            type="email"
            id="email"
            name="email"
            onChange={(e) => setEmail(e.target.value)}
            placeholder="Email *"
            value={email}
            autoComplete="off"
          />
          <span className="not-mail">Votre email n'est pas valide !</span>
        </div>
        <div className="form-group mb-2 py-2">
          <textarea
            id="message"
            name="message"
            rows="4"
            cols="50"
            onChange={(e) => setMessage(e.target.value)}
            placeholder="Message *"
            value={message}
          ></textarea>
        </div>
        <div className="d-flex justify-content-center mb-3">
          {showCaptcha && (
            <Recaptcha
              size="compact"
              data-theme="light"
              hl="fr"
              sitekey={RECAPTCHA_KEY}
              render="explicit"
              onloadCallback={recaptchaLoded}
              verifyCallback={verifyCallback}
            />
          )}
        </div>
        <input
          className="btn btn-sm font-weight-bold w-75"
          type="button"
          value="Envoyer"
          onClick={handleSubmit}
        />
        <div className="form_message mx-auto w-75"></div>
      </form>
    </div>
  );
};

export default ContactForm;
