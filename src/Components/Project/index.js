import React from "react";


const Project = ({pictures, name, handleShowDetails, index}) => {
  return (
    <div
      className="col-lg-4 col-md-6 portfolio-item filter-app"
      data-aos="fade-up"
      data-aos-duration="400"
      data-aos-easing="linear"
    >
      <div className="portfolio-wrap">
        <img src={pictures[0]} className="img-fluid" alt={`illustration du projet ${name}`} />
        <div className="portfolio-links">
          <span>{name}</span>
          <span onClick={() => handleShowDetails(index)}>En savoir plus</span>
        </div>
      </div>
    </div>
  );
};

export default Project;
