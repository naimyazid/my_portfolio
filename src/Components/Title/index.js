import React from "react";

const Title = ({children, paragraph}) => {
    return (
      <div className="section-title">
        <h2>{children}</h2>
        {paragraph ? <p>{paragraph}</p> : ""}
      </div>
    );
}

export default Title;