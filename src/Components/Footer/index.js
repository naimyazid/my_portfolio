import React from "react";
import { NavLink } from "react-router-dom";

const Footer = () => {
  return (
    <footer id="footer">
      <div className="container">
        <div className="author">
          <strong>
            <span>Naim Yazid - 2021</span>
            <NavLink exact to="/mentions légales" className="nav-link">
              Mentions légales.
            </NavLink>
          </strong>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
