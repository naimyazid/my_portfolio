import React from "react";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

const ProgressCircle = (props) => {
  return (
    <div className="box">
      <div className="box-icon">
        <CircularProgressbar
          value={props.percent}
          text={`${props.percent}%`}
          strokeWidth="15"
          styles={buildStyles({
            textColor: "#173b6c",
            fontSize: "18px",
          })}
        />
        <div className="icon">{props.children}</div>
      </div>
    </div>
  );
};

export default ProgressCircle;
