import React from "react";
import { FiMenu } from "react-icons/fi";
import { CgClose } from "react-icons/cg";

const MobileNavigation = ({ showNavSide, handleShowSide }) => {
  const buttonToggler = showNavSide ? <CgClose /> : <FiMenu />;
  const buttoTogglerStyle = showNavSide
    ? "mobile-nav-toggle mobile-nav-active d-xl-none"
    : "mobile-nav-toggle d-xl-none";

  return (
    <div
      className={buttoTogglerStyle}
      onClick={() => {
        handleShowSide();
      }}
    >
      {buttonToggler}
    </div>
  );
};

export default MobileNavigation;
