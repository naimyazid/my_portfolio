import React from "react";
import { BsGeoAltFill, BsEnvelopeFill, BsFillPhoneFill } from "react-icons/bs";

const Location = () => {
  return (
    <div
      className="col-lg-5 d-flex align-items-stretch"
      data-aos="fade-right"
      data-aos-duration="400"
      data-aos-easing="linear"
    >
      <div className="info">
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d41942.42725857099!2d2.330664154615045!3d48.9267901992233!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66eaae7346d61%3A0xc7efebbaf928e90d!2sSaint-Denis!5e0!3m2!1sfr!2sfr!4v1625869547291!5m2!1sfr!2sfr"
          frameBorder="0"
          allowFullScreen
          title="google-map"
          style={{
            border: "0",
            width: "100%",
            height: "290px",
            paddingBottom: "30px",
          }}
        ></iframe>

        <div className="contact_info_block">
          <div className="box-icon">
            <BsGeoAltFill />
          </div>
          <div className="text">
            <h6>Saint-Denis (Seine-Saint-Denis)</h6>
          </div>
        </div>

        <div className="contact_info_block">
          <div className="box-icon">
            <BsEnvelopeFill />
          </div>
          <div className="text">
            <h6>naimyazid@gmail.com</h6>
          </div>
        </div>

        <div className="contact_info_block">
          <div className="box-icon">
            <BsFillPhoneFill />
          </div>
          <div className="text">
            <h6>06 64 87 86 80</h6>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Location;
