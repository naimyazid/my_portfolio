import React from "react";
import { NavLink } from "react-router-dom";
import imageProfile from "../../media/photo_identite_mini.jpg";
import logo from "../../media/logo.png";
import { FaLinkedinIn } from "react-icons/fa";
import { FaGithub } from "react-icons/fa";
import { FiGitlab } from "react-icons/fi";
import Navigation from "../Navigation";

const Header = ({ handleShowSide }) => {
  return (
    <header
      className="header"
      onClick={() => {
        handleShowSide();
      }}
    >
      <div className="d-flex flex-column">
        <div className="profile">
          <img
            src={imageProfile}
            alt="profil"
            className="img-fluid rounded-circle"
          />
          <h1 className="text-light">
            <NavLink exact to="/" className="d-flex justify-content-center align-items-center">
              <span>Naim</span>
              <img
                className="img-fluid"
                src={logo}
                alt=""
              />
              <span>Yazid</span>
            </NavLink>
          </h1>
          <div className="social-links mt-3 text-center">
            <a
              href="https://www.linkedin.com/in/naim-yazid"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaLinkedinIn />
            </a>
            <a
              href="https://github.com/naimyazid"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaGithub />
            </a>
            <a
              href="https://gitlab.com/naimyazid"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FiGitlab />
            </a>
          </div>
        </div>

        <Navigation />
      </div>
    </header>
  );
};

export default Header;
