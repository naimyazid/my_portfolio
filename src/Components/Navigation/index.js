import React from "react";
import { NavLink } from "react-router-dom";
import { AiOutlineHome } from "react-icons/ai";
import { CgProfile } from "react-icons/cg";
import { AiOutlineFile } from "react-icons/ai";
import { GiGiftOfKnowledge } from "react-icons/gi";
import { BiBookContent } from "react-icons/bi";
import { AiOutlineMail } from "react-icons/ai";

const Navigation = () => {
  return (
    <nav id="navbar" className="nav-menu navbar">
      <ul>
        <li>
          <NavLink exact to="/" className="nav-link">
            <AiOutlineHome /> <span>Accueil</span>
          </NavLink>
        </li>
        <li>
          <NavLink exact to="a-propos" className="nav-link">
            <CgProfile /> <span>A propos</span>
          </NavLink>
        </li>
        <li>
          <NavLink exact to="parcours" className="nav-link">
            <AiOutlineFile /> <span>Parcours</span>
          </NavLink>
        </li>
        <li>
          <NavLink exact to="/competences" className="nav-link">
            <GiGiftOfKnowledge /> <span>Compétences</span>
          </NavLink>
        </li>
        <li>
          <NavLink exact to="/portfolio" className="nav-link">
            <BiBookContent /> <span>Projets</span>
          </NavLink>
        </li>
        <li>
          <NavLink exact to="/contact" className="nav-link">
            <AiOutlineMail /> <span>Contact</span>
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Navigation;
