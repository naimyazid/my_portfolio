import React from "react";
import { GiReturnArrow } from "react-icons/gi";

const ProjectDetails = ({ handleShowDetails, projectDetails }) => {
  const handlePanel = (e) => {
    const panels = document.querySelectorAll(".panel");
    panels.forEach((panel) => {
      panel.classList.remove("active");
    });
    e.target.classList.add("active");
  };

  return (
    <div>
      <section id="breadcrumbs">
        <div className="container">
          <div className="d-lg-flex justify-content-between align-items-center">
            <h2>{projectDetails.name}</h2>
            <ul>
              <li
                onClick={() => {
                  handleShowDetails();
                }}
              >
                <GiReturnArrow />
                <span className="ps-1">Projets</span>
              </li>
              <li>Détail du projet</li>
            </ul>
          </div>
        </div>
      </section>

      <section className="portfolio-details">
        <div className="container">
          <div className="row gy-4">
            <div
              className="col-lg-7"
              data-aos="fade-right"
              data-aos-easing="linear"
              data-aos-duration="400"
            >
              <div className="d-flex align-items-center">
                {projectDetails.pictures.map((picture, index) => (
                  <div
                    className={index === 0 ? "panel active" : "panel"}
                    key={index}
                    style={{
                      backgroundImage: `url(${picture})`,
                      width: "100%",
                    }}
                    onClick={(e) => {
                      handlePanel(e);
                    }}
                  ></div>
                ))}
              </div>
            </div>

            <div
              className="col-lg-5"
              data-aos="fade-left"
              data-aos-easing="linear"
              data-aos-duration="400"
            >
              <div className="portfolio-info">
                <h3>Inforimations sur le projet</h3>
                <ul>
                  {projectDetails.source ? (
                    <li>
                      <strong>Code source : </strong>
                      <a
                        href={projectDetails.source}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {projectDetails.source}
                      </a>
                    </li>
                  ) : (
                    ""
                  )}
                  <li>
                    <strong>Liens du projet : </strong>
                    <a
                      href={projectDetails.site}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {projectDetails.site}
                    </a>
                  </li>
                  <li className="languages">
                    <strong>Technos : </strong>
                    {projectDetails.technos.map((language, index) => (
                      <span className="badge" key={index}>
                        {language}
                      </span>
                    ))}
                  </li>
                </ul>
              </div>
              <div className="portfolio-description">
                <h2>Déscription :</h2>
                <p>{projectDetails.info}</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default ProjectDetails;
