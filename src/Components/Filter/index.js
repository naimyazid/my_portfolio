import React from "react";

const Filter = ({filtersArray, handleFilter, filter}) => {

  return (
    <div className="row" data-aos="fade-up">
      <div className="col-lg-12 d-flex justify-content-center">
        <ul className="portfolio-flters">
          {filtersArray.map((filterItem) => (
            <li
              key={filterItem.id}
              data-filter={filterItem.value}
              onClick={(e) => {
                handleFilter(e);
              }}
              className={filterItem.value === filter ? "filter-active" : ""}
            >
              {filterItem.value}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default Filter;
