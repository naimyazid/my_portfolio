import { React, useState, useEffect } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Header from "../Header";
import Home from "../../pages/Home";
import About from "../../pages/About";
import EducationAndExperince from "../../pages/EducationAndExperience";
import Knowledges from "../../pages/Knowledges";
import Portfolio from "../../pages/Portfolio";
import Contact from "../../pages/contact";
import Footer from "../Footer";
import MobileNavigation from "../MobileNavigation";
import AOS from "aos";
import "aos/dist/aos.css";
import LegalNotice from "../../pages/LegalNotice";
import Cookie from "../Coockie";

const App = () => {
  AOS.init();
  const [showNavSide, setShowNavSide] = useState(false);
  const [loading, setLoading] = useState(true);

  const hideLoader = () => {
    return new Promise((resolve) => setTimeout(() => resolve(), 2500));
  };

  useEffect(() => {
    hideLoader().then(() => setLoading(false));
  }, []);

  const handleShowSide = () => {
    setShowNavSide(!showNavSide);
  };

  const scrollToTop = () => {
    const backToTopElement = document.querySelector(".back-to-top");
    let elementPosition = 0;

    if (backToTopElement) {
      elementPosition = backToTopElement.offsetHeight;
    }

    if (elementPosition > 0) {
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    }
  };

  setTimeout(()=>scrollToTop(), 400);

  return loading ? (
    ""
  ) : (
    <main id="main" className="back-to-top">
      <BrowserRouter>
        <MobileNavigation
          showNavSide={showNavSide}
          handleShowSide={handleShowSide}
        />
        <Header handleShowSide={handleShowSide} />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/a-propos" component={About} />
          <Route path="/parcours" component={EducationAndExperince} />
          <Route path="/competences" component={Knowledges} />
          <Route path="/portfolio" exact component={Portfolio} />
          <Route path="/contact" exact component={Contact} />
          <Route path="/mentions légales" exact component={LegalNotice} />
          <Redirect to="/" />
        </Switch>
        <Footer />
        <Cookie />
      </BrowserRouter>
    </main>
  );
};

export default App;
